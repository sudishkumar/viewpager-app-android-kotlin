package com.edugaon.viewpagerapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val secondViewPager = findViewById<ViewPager>(R.id.myViewpager)

        val myAdapter = SecondViewPagerAdapter(supportFragmentManager)

        myAdapter.addFragment(TeachersFragment(), "Teachers")
        myAdapter.addFragment(StudentsFragment(), "Students")

        secondViewPager.adapter  = myAdapter
    }

    class SecondViewPagerAdapter(fragment: FragmentManager) : FragmentPagerAdapter(fragment){
        private var fragmentList: MutableList<Fragment> = ArrayList()
        private var titleList : MutableList<String> = ArrayList()

        override fun getCount(): Int {
            return fragmentList.size
        }

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        fun addFragment(fragment: Fragment, title: String){
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }
    }
}